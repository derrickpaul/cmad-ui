#!/bin/bash

echo "Get credentials for ui-cluster"
gcloud container clusters get-credentials ui-cluster --zone us-central1-a --project igneous-gamma-163908
