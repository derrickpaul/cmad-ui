#!/bin/bash

echo "Resize ui-cluster to two instances"
gcloud container clusters resize ui-cluster --size=2 --zone=us-central1-a