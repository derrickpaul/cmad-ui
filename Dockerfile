FROM node:6.11

# Run the 'npm install' and 'npm run build' commands on the host before triggering a docker build. 
ADD build/ /opt/build/

WORKDIR /opt/

RUN chmod -R 755 /opt/ && \
	npm install -g serve

EXPOSE 80

CMD ["serve", "-s", "-n", "-p", "80", "/opt/build"]