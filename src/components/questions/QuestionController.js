import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as questionActions from '../../actions/questionActions';
import * as initialState from '../../reducers/initialState';

import QuestionPage from './QuestionPage';

class QuestionController extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onPostAnswer = this.onPostAnswer.bind(this);
    }

    componentDidMount() {
        this.props.actions.loadQuestionById(this.props.match.params.questionId);
    }

    onPostAnswer(answer) {
        this.props.actions.postAnswer(this.props.question._id, answer);
    }

    render() {
        return (
            <div className="container">
                <QuestionPage hideQuestionEditor={true} question={this.props.question} onPostAnswer={this.onPostAnswer}/>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        question: state.questions.question
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions : bindActionCreators(questionActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionController);