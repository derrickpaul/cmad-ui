import React from 'react';
import QuestionTag from './QuestionTag';

function returnHtml(text) {
  return {__html: text};
}

const QuestionRow = ({question}) => {
    var text = question.text.substring(0, 100);
    if (question.text.length > 100) {
        text+= " ...";
    }

    return (
    
    <div>
        <div className="row entry">
            <div className="col-xs-1 vote-padding text-muted">
                <div className="col-center">
                    {question.voteCount}
                </div>
                <div className="col-center">
                    <p>votes</p>
                </div>
                <div className="col-center">
                    { 'undefined' === typeof question.answers ? 0 : question.answers.length}
                </div>
                <div className="col-center ">
                    <p>answers</p>
                </div>
                <div className="col-center ">
                    <p>{question.viewCount} views</p>
                </div>
            </div>
            <div className="col-xs-11">
                <div className="question-row">
                    <a href={"/questions/" + question._id} className="list-group-item-heading">{question.title}</a>
                </div>
                <div className="question-row"> 
                    {/*<p className="list-group-item-text" dangerouslySetInnerHTML={returnHtml(question.text)}/>*/}
                    <p className="list-group-item-text" >{text}</p>
                </div>

                <div className="question-row">
                    {'undefined' !== typeof question.tags && question.tags.map((tag) => {
                        return <span className="label label-info label-item">{tag}</span>
                    })}
                </div>      
            </div>
        </div>    
    </div>
    );};

export default QuestionRow;