import React from 'react';
import {connect} from 'react-redux';

class QuestionPagination extends React.Component {
   constructor(props, context) {
       super(props, context);

       this.state = {
           skip: 0,
           limit: 3,
       }

       this.getPreviousClass = this.getPreviousClass.bind(this);
       this.getNextClass = this.getNextClass.bind(this);
       this.getPage = this.getPage.bind(this);
       this.getPages = this.getPages.bind(this);
       this.onAskQuestion = this.onAskQuestion.bind(this);
   }

   getPreviousClass() {
       return this.state.skip == 0 ? "disabled" : "";
   }

   getNextClass() {
       return (this.props.questions.total / this.state.limit) <= this.state.skip + 1 ? "disabled" : "";
   }

   onAskQuestion(){
        window.location = '/ask/';
   }

   handlePageClick(page, event) {
        var state = Object.assign({}, this.state);
        state['skip'] = page;
        this.setState(state);
        this.props.onClick(page);
   }

   getPage(isActive, page) {
       return (<li className={isActive} onClick={this.handlePageClick.bind(this, page)} >
                    <span>{page + 1}<span className="sr-only">(current)</span></span>
                </li>);
   }

   getPages() {
       var pages = this.props.questions.total / this.state.limit;
       var activePage = this.state.skip;
       var rows = []
       var page;
       for (page = 0 ; page < pages; page ++) {
           var isActive = page == this.state.skip ? "active" : "";
           rows.push(this.getPage(isActive, page));
       }
       return rows;
   }

   render() {
       return(
            <nav aria-label="Page navigation">
                <div >
                    <ul className="pagination">
                        <li className={this.getPreviousClass()} onClick={this.handlePageClick.bind(this, this.state.skip - 1)}>
                            <span><span aria-hidden="true">&laquo;</span></span>
                        </li>
                        {this.getPages()}
                        <li className={this.getNextClass()} onClick={this.handlePageClick.bind(this, this.state.skip + 1)}>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                    <ul className="pagination pull-right">
                        <li>
                        <button className="btn btn-primary" type="submit" onClick={this.onAskQuestion}>Ask Question</button>    
                        </li>
                    </ul>
                </div>
            </nav>   
       );
   }
}

function mapStateToProps(state, ownProps) {
    return {
        questions: state.questions
    };
}

export default connect(mapStateToProps)(QuestionPagination);