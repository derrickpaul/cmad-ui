import React from 'react';
import QuestionRow from './QuestionRow';

const QuestionsList = ({questions}) => {
    return (
        <div className="list-group container-fluid">
            {questions.map(question => 
                <QuestionRow key={question._id} question={question}/>
            )}
        </div>
    );
};

export default QuestionsList;