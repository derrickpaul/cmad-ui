import React from 'react';
import TrumboWrap from '../common/TrumboWrap';
import TagInput from '../common/TagInput';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as questionActions from '../../actions/questionActions';
import 'react-bootstrap-typeahead/css/Token.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import * as types from '../../actions/actionTypes';
import toastr from 'toastr';

class AskQuestionPage extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            question: {
                title: "",
                text: "",
                tags: []
            },
            saving: false
        }

        this.onTextChange = this.onTextChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.onTagsChange = this.onTagsChange.bind(this);
        this.onPostQuestion = this.onPostQuestion.bind(this);
        this.copyDeep = this.copyDeep.bind(this);
    }

    componentWillReceiveProps(newProps){
        if( newProps.action === types.CREATE_QUESTION_SUCCESS && 'undefined' !== typeof newProps.questionPath) {

            this.props.history.push('/questions/' + newProps.questionPath);
            // this.context.router.replace('/questions/' + newProps.questionPath);        
        }
    }

    onTextChange(text) {
        var state = this.copyDeep(this.state);
        state.question.text = text;
        this.setState(state);
    }

    onTitleChange(event) {
        var state = this.copyDeep(this.state);
        state.question.title = event.target.value;
        this.setState(state);
    }

    onTagsChange(tags) {
        var newTags = [];
        tags.forEach(function(tag) {
            if (typeof tag === 'object') {
                newTags.push(tag.name);
            } else {
                newTags.push(tag);
            }
        }, this);
        var state = this.copyDeep(this.state);
        state.question.tags = newTags;
        this.setState(state);
    }

    onPostQuestion(event) {
        var state = this.copyDeep(this.state)
        state.saving = true;
        this.props.actions.askQuestion(this.state.question).then((questionPath) => {
            this.setState({saving:false});
        }).catch( error => {
            var state = this.copyDeep(this.state)
            state.saving = false;
            this.setState(state);
            });
    }

    copyDeep(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    render() {
        var saving = this.state.saving;
        return (
            <div className="container">
                <div className="row entry">
                    <form>
                        <div className="form-group">
                            <label for="questionTitle">Title</label>
                            <input type="text" className="form-control" id="questionTitle" aria-describedby="titleHelp" onChange={this.onTitleChange} placeholder="Enter your question title here..."/>
                            <small id="titleHelp" className="form-text text-muted">Enter a short descriptive title for your question.</small>
                        </div>
                        <div className="ask-question-input"> 
                            <TrumboWrap id={new Date().getMilliseconds().toString()} onChange={this.onTextChange} data=""/>
                        </div>
                            
                    </form>
                </div>
                <div className="ask-question-input"> 
                      <TagInput onChange={this.onTagsChange}/>      
                </div>
                <button 
                type="button" 
                className="btn btn-info ask-question-input" 
                onClick={this.onPostQuestion} 
                disabled={saving}>
                { saving ? 'Saving...': 'Post Your Question'}
                </button>
            </div>
        );
    }

}

function mapStateToProps(state,ownProps) {
    return {
        questionPath: state.questions.questionPath,
        action: state.ajaxStatus.action
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(questionActions, dispatch)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(AskQuestionPage);