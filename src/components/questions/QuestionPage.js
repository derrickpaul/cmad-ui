import React from 'react';
import * as types from '../common/entryTypes';

import EntryController from '../common/EntryController';
import TrumboWrap from '../common/TrumboWrap';

class QuestionPage extends React.Component {

   constructor(props, context) {
       super(props, context);

       this.state = {
           answer: ""
       }

       this.onAnswerTextChange = this.onAnswerTextChange.bind(this);
       this.copyDeep = this.copyDeep.bind(this);
       this.onPostAnswer = this.onPostAnswer.bind(this);
   }
   
   onAnswerTextChange(answer) {
        var state = this.copyDeep(this.state);
        state.answer = answer;
        this.setState(state);
   }

   onPostAnswer() {
       this.props.onPostAnswer(this.state.answer);
   }

   copyDeep(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

   render() {
       // TODO: move out to .css file.
       var headerStyle ={
           'border-bottom': '.05rem solid #e5e5e5'
       }

       var question = this.props.question;
       var questionId = this.props.question._id;
       var answer = this.state.answer;

       return (
            <div >
                {/*<div className="header clearfix" style={headerStyle}>*/}
                <div className="header clearfix" >
                    <h4 className="text-muted">{this.props.question.title}</h4>
                </div>
                <EntryController entry={question} type={types.QUESTION}/>
                {this.props.question.answers.map((answer) => {
                    var answerId = answer.answerId;
                    return <EntryController entry={answer} type={types.ANSWER} questionId={question._id}/>
                })}

                <div className="row entry">
                    <h4>Your Answer</h4>
                    <TrumboWrap id={new Date().getMilliseconds().toString()} onChange={this.onAnswerTextChange} data={answer}/>
                    <button type="button" className="btn btn-info" onClick={this.onPostAnswer}>Post Your Answer</button>
                </div>
                
            </div>   
       );
   }
}

export default QuestionPage;