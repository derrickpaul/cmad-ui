import React from 'react';

const QuestionTag = (tag) => {
    return (
        <span className="label label-primary" key={tag}>{tag}</span>
    );
}

export default QuestionTag;