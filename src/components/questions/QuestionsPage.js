import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as questionActions from '../../actions/questionActions';
import QuestionsList from './QuestionsList';
import QuestionPagination from './QuestionPagination';

class QuestionsPage extends React.Component {
   constructor(props, context) {
       super(props, context);

       this.state = {
           selectedTab: "NEWEST",
           selectedPage: 0, 
           skip: 0,
           limit: 3,
           questionSearchTerm: "",
       }

       this.onClickNewest = this.onClickNewest.bind(this);
       this.onClickVotes = this.onClickVotes.bind(this);
       this.onClickViewed = this.onClickViewed.bind(this);
       this.isActive = this.isActive.bind(this);
       this.onPageClick = this.onPageClick.bind(this);
       this.onClickSearchTab = this.onClickSearchTab.bind(this);
       this.onSearchTextChange = this.onSearchTextChange.bind(this);
       this.copyDeep = this.copyDeep.bind(this);
       this.onSearchTextChange = this.onSearchTextChange.bind(this);
       this.onSearchClick = this.onSearchClick.bind(this);
   }
   
   componentDidMount() {
        this.props.actions.loadNewestQuestions();
   }

   onClickTab(tabName) {
        var state = Object.assign({}, this.state);
        state["selectedTab"] = tabName;
        this.setState(state);
   }

   onClickNewest(event) {
        this.onClickTab("NEWEST");
        this.props.actions.loadNewestQuestions(this.state.skip);
   }

   onClickVotes(event) {
        this.onClickTab("VOTES");
        this.props.actions.loadVotedQuestions(this.state.skip);
   }

   onClickViewed(event) {
        this.onClickTab("VIEWED");
        this.props.actions.loadMostViewedQuestions(this.state.skip);
   }

   onClickSearchTab(event) {
        this.onClickTab("SEARCH");
        var query = this.state.questionSearchTerm;
        if ('undefined' !== typeof query && query.length > 0) {
             this.props.actions.searchQuestions(query);
        } else {
            this.forceUpdate();
        }
   }

   onSearchTextChange(event) {
        var text = event.target.value
        var state = this.copyDeep(this.state);
        state.questionSearchTerm = text;
        this.setState(state);
   }

   onSearchClick(event) {
        var query = this.state.questionSearchTerm;
        this.props.actions.searchQuestions(query);
   }

   copyDeep(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

   isActive(tabName) {
        return tabName === this.state.selectedTab ? "active" : "";
   }

   onPageClick(skip) {
       var state = Object.assign({}, this.state);
       state['skip'] = skip;
       this.setState(state);
        switch(this.state.selectedTab) {
            
            case 'NEWEST':
            this.props.actions.loadNewestQuestions(skip);
            break;
            
            case 'VOTES':
            this.props.actions.loadVotedQuestions(skip);
            break;

            case 'VIEWED':
            this.props.actions.loadMostViewedQuestions(skip);
            break;

            default :
            var query = this.state.questionSearchTerm;
            this.props.actions.searchQuestions(query, skip);
            break;
        }
   }

   render() {
       return (
            <div className="container">
            <div className="container-fluid">
                <nav>
                    <ul className="nav nav-tabs">
                        <li role="presentation" className={this.isActive('NEWEST')} ><a onClick={this.onClickNewest}>Newest</a></li>
                        <li role="presentation" className={this.isActive('VOTES')}><a onClick={this.onClickVotes}>Votes</a></li>
                        <li role="presentation" className={this.isActive('VIEWED')}><a onClick={this.onClickViewed}>Viewed</a></li>
                        <li role="presentation" className={this.isActive('SEARCH')}><a onClick={this.onClickSearchTab}>Search</a></li>
                    </ul>
                    {   this.isActive('SEARCH') &&
                        <div className="form-inline">
                            <input className="form-control mr-sm-2" type="text" placeholder="Search" onChange={this.onSearchTextChange}/>
                            <button 
                            className="btn btn-outline-secondary my-2 my-sm-0"  
                            disabled={this.state.questionSearchTerm.length == 0}
                            onClick={this.onSearchClick}>
                            Search
                            </button> 
                        </div>
                    }
                </nav> 
                <QuestionsList questions={this.props.questions.data}/>
                <QuestionPagination onClick={this.onPageClick}/>
            </div>   
            </div>    
       );
   }
}

// TODO: Add prop types

function mapStateToProps(state, ownProps) {
    return {
        questions: state.questions
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions : bindActionCreators(questionActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsPage);