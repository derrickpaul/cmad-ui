import React from 'react';

class UserInfo extends React.Component {
   render() {
       return(
            <div className="media user-info">
                <div className="media-left">
                    <img className="media-object image-shadow" src="/gravatar.png" alt={this.props.userId}/>
                </div>
                <div className="media-body">
                    <a href={"/user/" + this.props.userId}>{this.props.userId}</a>
                    <p>{ (this.props.isQuestion ? "asked on " : "answered on ") + new Date(this.props.date).toDateString() }</p>
                </div>
            </div> 
       );
   }
}

export default UserInfo;
