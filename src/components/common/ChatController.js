import React from 'react';
import CmadChat from './CmadChat';
import Session from '../../store/session';
import Sidebar from 'react-sidebar';
import $ from 'jquery-ui';
import * as userActions from '../../actions/userActions';
import AjaxHelper from '../../api/ajaxHelper';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class ChatController extends React.Component{
    constructor(props, context) {
        super(props, context)

        this.state = {
            conversations: [],
            selectedUser: "",
            inProgressText: "",
            users: []
        };

        this.onSelectionChange = this.onSelectionChange.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.addToConversations = this.addToConversations.bind(this);
        this.copyDeep = this.copyDeep.bind(this);
        this.sendChat = this.sendChat.bind(this);
        this.chatReceived = this.chatReceived.bind(this);
        this.getConversation = this.getConversation.bind(this);
        this.getFormattedConversation = this.getFormattedConversation.bind(this);
        this.getFormattedMessage = this.getFormattedMessage.bind(this);
        this.getUserNameFormatted = this.getUserNameFormatted.bind(this);
        this.keyUp = this.keyUp.bind(this);
    }

    componentWillReceiveProps(newProps) {
        var state = this.copyDeep(this.state);
        state.users = newProps.users;
        this.setState(state);
    }

    componentDidMount() {
        // load users here.
        var user = Session.getUser();
        if ('undefined' === typeof user || 'undefined' === user) {
            window.localStorage = "/login";
        }
        this.cmadChat = new CmadChat(AjaxHelper.getChatUrl(), user.userName, 60);
        this.cmadChat.addChatMessageListener(this.chatReceived);
        this.cmadChat.addPresenceMessageListener(this.presenceRequestReceived);
        this.cmadChat.connect();
        this.props.actions.loadUsers();
    }

    onSelectionChange(userName) {
        var state = this.copyDeep(this.state);
        state.selectedUser = userName;
        var conversation = this.getConversation(state, userName);
        var history = conversation.history;
        if ('undefined' !== typeof history) {
            history.forEach((item) => {
                item.viewed = true;
            });
        }
        this.setState(state);
    }

    onTextChange(event) {
        event.preventDefault();
        var text = event.target.value;

        var state = this.copyDeep(this.state);
        state.inProgressText = text;

        this.setState(state);
    }

    chatReceived(message) {
        var state = this.copyDeep(this.state);

        var historyItem = {
                message: message.message,
                viewed: this.state.selectedUser === message.fromUserName,
                time: new Date().toDateString(),
                from: 1
        }

        this.addToConversations(state, historyItem, message.fromUserName);
        this.setState(state);
    }

    addToConversations(state, message, userName) {
        var conversation = this.getConversation(state, userName);
        conversation.history.push(message);
    }

    getConversation(state, userName) {
        var conversations = state.conversations;
        var conversation = {
                userName,
                history : []
            }

        var onGoingConversations = conversations.filter((conversation) => {
            return conversation.userName === userName;
        });

        if (onGoingConversations.length === 1) {
            conversation = onGoingConversations[0];
        } else {
             conversations.push(conversation);
        }
        return conversation;
    }

    sendChat() {
        debugger;
        var text = this.state.inProgressText;
        var state = this.copyDeep(this.state);

        if ('undefined' != typeof text && text.length > 0) {
                var message = {
                message: text,
                viewed: true,
                time: new Date().toDateString(),
                from: 0
            }

            var selectedUser = this.state.selectedUser;
            this.addToConversations(state, message, selectedUser);
            state.inProgressText = "";
            this.setState(state);
            this.cmadChat.sendChat(this.state.selectedUser, text);
        }
    }
    
    presenceRequestReceived(message) {
        return ({
            userName: Session.getUser(),
            presence: "ONLINE",
            type: "USER_STATUS"
        });
    }

    copyDeep(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    getUser(userName) {
        var filtered = this.state.users.filter((user) => {
            return user.userName === userName;
        })
        if (filtered.length === 1) {
            return filtered[0];
        }
    }

    getUserNameFormatted(userName) {
        var userObj = this.getUser(userName);
        if ('undefined' !== typeof userObj) {
            return userObj.firstName + " " + userObj.lastName;
        }
    }

    renderUsers() {
        return(

        <ul className="list-group">
            { this.state.users.map((user) => {
                var state = this.copyDeep(this.state);
                var conversation = this.getConversation(state, user.userName);
                var unRead = 0;
                if ('undefined' !== typeof conversation.history) {
                    unRead = conversation.history.filter((item) => {
                        return !item.viewed;
                    }).length;
                }

                return <li className={"list-group-item " + (( user.userName === this.state.selectedUser) &&  "active")}
                key={user.userName} 
                qtip={user.userName}
                onClick={this.onSelectionChange.bind(this, user.userName)}>{this.getUserNameFormatted(user.userName)}
                {unRead > 0 && <span className="badge">{unRead}</span>}
                </li>
            }) }
        </ul>
        )
    }

    getFormattedConversation() {
        var userName = this.state.selectedUser;
        var conversation = this.getConversation(this.state, userName);
        var text = "";
        var that = this;
        if ('undefined' != typeof conversation) {
            conversation.history.forEach(function(message) {
                text+= that.getFormattedMessage(message, userName) + "\n"
            });
        }
        return text;
    }

    getFormattedMessage(message, userName) {
        var text = "";
        if (message.from === 1) {
            text = userName + " > " + message.message;
        } else {
            text = Session.getUser().firstName + " > " + message.message;
        }
        return text;
    }

    keyUp(event) {
        if (event.keyCode === 13) {
            this.sendChat();
        }
    }

    render() {
        return (
            <div className="container">
                <div className="col-lg-7">
                    <div>
                        <div className="form-group">
                            <textarea 
                            rows="10"
                            readonly
                            type="text" 
                            className="form-control chat-window" 
                            id="chatText" 
                            aria-describedby="titleHelp" 
                            value={this.getFormattedConversation()}
                            />
                        </div>
                        <div className="form-group">
                            <input 
                            className="form-control input-chat" 
                            type="text" 
                            placeholder="Your message..." 
                            onChange={this.onTextChange}
                            value={this.state.inProgressText}
                            onKeyUp={this.keyUp}/>
                        </div>
                        <div className="form-group">  
                            <button 
                            className="form control btn btn-primary pull-right"  
                            disabled={this.state.inProgressText.length == 0 || this.state.selectedUser.length == 0}
                            onClick={this.sendChat}>
                            Send
                            </button>
                        </div>
                            
                    </div>
                </div>
                <div className="limit">
                    <div className="col-sm-3 vote-padding">
                        { this.state.users.length > 0 && this.renderUsers()}
                    </div>
                </div>
            </div>
            
        );
    }
}

function mapStateToProps(state,ownProps) {
    return {
        users: state.users
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions : bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatController);
