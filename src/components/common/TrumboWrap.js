import React from 'react';
import 'react-trumbowyg/dist/trumbowyg.min.css';
import Trumbowyg from 'react-trumbowyg';


class TrumboWrap extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            text: ""
        }

        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.setState({
            text: event.target.innerHTML
        });
        this.props.onChange(event.target.innerHTML);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.text === this.state.text) {
            return false;
        }
        return true;
    }

    render() {
        var text = this.props.text;

        return ( 
            <div>
                <Trumbowyg 
                id={new Date().getMilliseconds().toString()} 
                onChange={this.onChange} 
                data={'undefined' !== typeof text? text : ""} />
            </div>
        );
    }

}

export default TrumboWrap;