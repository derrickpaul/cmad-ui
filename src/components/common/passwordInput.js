import React from 'react';
import PropTypes from 'prop-types'


const PasswordInput = ({name,label,onChange, placeholder,value,error,disabled}) =>  {
    let wrapperClass = 'form-group';
    if(error && error.length > 0) {
        wrapperClass += " " + 'has-error';
    }
    return(
        <div className={wrapperClass}>
            <label htmlFor={name} className="control-label">{label} </label> 
            <div className ="field">
                <input
                    type = "password"
                    name ={name}
                    className = 'form-control'
                    placeholder={placeholder}
                    value={value}
                    disabled = {disabled ? true : false}
                    onChange={onChange} />
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    );
};

PasswordInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string,
    error: PropTypes.string
};

export default PasswordInput;
