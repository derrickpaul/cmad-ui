import React from 'react';
import TrumboWrap from './TrumboWrap';

class EntryText extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            hide: true
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.hide !== 'undefined') {
            if (newProps.hide) {
                this.setState({
                    hide: true
                })
            } else {
                this.setState({
                    hide: false
                })
            }
        }
    }

    returnHtml(text) {
        return {__html: text};
    }
    
    render() {
        var text = this.props.text;

        return ( 
            <div>
                <p className="row " dangerouslySetInnerHTML={this.returnHtml(text)}/>
                <div className="row">
                {this.state.hide || <TrumboWrap onChange={this.props.onChange} text={text}/>}
                </div>
            </div>
        );
    }
}

export default EntryText;