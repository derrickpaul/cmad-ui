import React from 'react';
import Comment from './Comment';
import UserInfo from './UserInfo';
import EntryText from './EntryText';
import * as types from './entryTypes';

import * as initialState from '../../reducers/initialState';

class Entry extends React.Component {
    constructor(props, context) {
       super(props, context);

       this.state = {
           hideEntryEditor : true,
           toggleEdit: "edit",
           entry: initialState.InitialState.question,
           hideAddComment: true,
           toggleAddComment: "add comment",
           comment: ""
       }

       this.renderIfDefined = this.renderIfDefined.bind(this);
       this.renderVotes = this.renderVotes.bind(this);
       this.renderUser = this.renderUser.bind(this);
       this.renderComments = this.renderComments.bind(this);
       this.renderAddComment = this.renderAddComment.bind(this);
       this.renderTags = this.renderTags.bind(this);
       this.onVoteUp = this.onVoteUp.bind(this);
       this.onVoteDown = this.onVoteDown.bind(this);
       this.onTextChange = this.onTextChange.bind(this);
       this.onEditEntry = this.onEditEntry.bind(this);
       this.setStateFromProps = this.setStateFromProps.bind(this);
       this.onSaveText = this.onSaveText.bind(this);
       this.onAddComment = this.onAddComment.bind(this);
       this.onCommentTextChange = this.onCommentTextChange.bind(this);
       this.onSaveComment = this.onSaveComment.bind(this);
       this.renderCommentsForQuestion = this.renderCommentsForQuestion.bind(this);
       this.renderCommentsForAnswers = this.renderCommentsForAnswers.bind(this);
    }

    componentDidMount(){
        this.setStateFromProps(this.props);
    }

    componentWillReceiveProps(newProps) {
        this.setStateFromProps(newProps);
    }

    setStateFromProps(props) {
        var state = this.copyDeep(this.state);
        var entry = this.copyDeep(props.entry);
        state.entry = entry;
        this.setState(state);
    }

    copyDeep(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    renderIfDefined(prop,renderFunction) {
        if (typeof prop !== 'undefined') {
            return renderFunction();
        }
    }

    onVoteUp(event) {
        var entryId = this.props.entryId;
        var entryType = this.props.entryType;

        this.props.onVoteUp(entryId, entryType, event);
    }

    onVoteDown(event) {
        var entryId = this.props.entryId;
        var entryType = this.props.entryType;

        this.props.onVoteDown(entryId, entryType, event);
    }

    onTextChange(text, event) {
        var state = this.copyDeep(this.state);
        state.entry.text = text;
        this.setState(state);
    }

    onEditEntry() {
        var state = this.copyDeep(this.state);
        state.hideEntryEditor = !this.state.hideEntryEditor;
        state.toggleEdit = this.state.hideEntryEditor ? 'cancel' : 'edit';
        if (state.toggleEdit === 'edit') {
            state.entry.text = this.props.entry.text;
        }
        this.setState(state);
    }

    onSaveText() {
        this.onEditEntry();
        this.props.onSaveText(this.state.entry.text);
    }

    onCommentTextChange(event) {
        var comment = event.target.value;
        var state = this.copyDeep(this.state);
        state.comment = comment;
        this.setState(state);
    }

    onAddComment(event) {
        var state = this.copyDeep(this.state);
        state.hideAddComment = !this.state.hideAddComment;
        state.toggleAddComment = state.hideAddComment ? "add comment" : "cancel";
        if (state.toggleAddComment === 'cancel') {
            state.comment = "";
        }
        this.setState(state);
    }

    onSaveComment(event) {
        this.onAddComment(event);
        this.props.onSaveComment(this.state.comment);
    }

    renderVotes() {
        var voteCount = this.props.entry.voteCount;

        return (
            <div>
                <div className="row col-center">
                    <span onClick={this.onVoteUp} className="glyphicon glyphicon-triangle-top text-muted" aria-hidden="true"></span>
                </div>
                <div className="row col-center">
                    <span className="text-muted">{voteCount}</span>
                </div>
                <div className="row col-center">
                    <span onClick={this.onVoteDown} className="glyphicon glyphicon-triangle-bottom text-muted" aria-hidden="true"></span>
                </div>
            </div>
        );
    }

    renderUser() {
        return (
            <div className="row label-group ">
                <ul className="list-inline">
                    <li className="list-inline-item"><a >share</a></li>
                    <li className="list-inline-item"><a onClick={this.onEditEntry}>{this.state.toggleEdit}</a></li>
                    {this.state.toggleEdit === 'cancel' && <li className="list-inline-item"><a onClick={this.onSaveText}>save</a></li>}
                </ul>
                <div className="col-lg-4 pull-right">
                    <UserInfo 
                    userId={this.props.entry.userId} 
                    date={this.props.entry.createdTime}
                    isQuestion={this.props.type === types.QUESTION}/>
                </div>
            </div>
        );
    }

    renderCommentsForQuestion() {
        return (
            <div>
                {this.props.entry.comments.map((comment) => {
                    return  <Comment comment={comment} /> 
                })} 
            </div>            
        );
    }

    renderCommentsForAnswers() {
        var comments = this.props.entry.comments;
        return (
        <div>    
            {Object.keys(comments).map(function (key) {
            var comment = comments[key];
            return  <Comment comment={comment} /> 
        })}
        </div>
        );
    }

    renderComments() {
        return (
            <div>
                {this.props.type === types.QUESTION ? this.renderCommentsForQuestion() : this.renderCommentsForAnswers()}
            </div>
        );
    }

    renderAddComment() {
        var addComment = this.state.toggleAddComment;
        return (
            <div className="row add-comment">
                    <ul className="list-inline">
                        <li className="list-inline-item"><a onClick={this.onAddComment}>{addComment}</a></li>
                        {!this.state.hideAddComment && <li className="list-inline-item" onClick={this.onSaveComment}><a>save</a></li>}
                    </ul>
                {!this.state.hideAddComment && <textarea className="form-control" onChange={this.onCommentTextChange}/>}
            </div>
        );
    }

    renderTags() {
        return (
            <div className="row label-group">
                {this.props.entry.tags.map((tag) => {
                    return <span className="label label-info label-item">{tag}</span>
                })}
            </div>
        );
    }

    returnHtml(text) {
        return {__html: text};
    }

    render() {
        var text = this.state.entry.text;
        var tags = this.props.entry.tags;
        var comments = this.props.entry.comments;

        return (
            <div className="row entry">
                <div className="col-xs-1 vote-padding">
                    {this.renderVotes()}
                </div>
                <div className="col-xs-11">
                    <EntryText text={text} onChange={this.onTextChange} hide={this.state.hideEntryEditor}/>
                    {this.renderIfDefined(tags, this.renderTags)}
                    {this.renderUser()}
                    <div className="row label-group border-bottom"/>
                    {this.renderIfDefined(comments, this.renderComments)}
                    {this.renderAddComment()}

                </div>
            </div>   
        );
    }
}

export default Entry;