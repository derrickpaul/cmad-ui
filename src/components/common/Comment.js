import React from 'react';

class Comment extends React.Component {
    constructor(props, context) {
       super(props, context);

       this.returnHtml = this.returnHtml.bind(this);
    }

    returnHtml(text) {
        return {__html: text};
    }

    render() {
        var text = this.props.comment.text;
        return (
            <div className="row comment">
                <span>
                <span dangerouslySetInnerHTML={this.returnHtml(text)}/>
                <a href={"/user/" + this.props.comment.userId}>{" - " + this.props.comment.userId}</a>
                <span>{ "  " + new Date(this.props.comment.createdTime).toDateString()}</span>
                </span>          
            </div>    
        );
    }
}

export default Comment;