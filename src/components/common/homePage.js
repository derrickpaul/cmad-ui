import React from 'react';
import {Link} from 'react-router-dom';

class HomePage extends React.Component {
    render(){
        return (
			<div className="container">                    
              <div className="row">
                <div className="col-lg-12">
                    <h1 className="page-header">
                        <small>Home</small>
                    </h1>
                 </div>
              </div>                    
              <div className="row">
                    <div className="col-md-12">
                        <div class="image-wrap homescreen"></div>
                        <img className="img-responsive" src="/img/background.jpg" alt="" />
                    </div>
                    <div className="col-md-6">
                        <h3>HeapOverflow</h3>
                        <p>
                            Founded in 2017, Heap Overflow is the up coming, good online community for developers to learn, share their knowledge, and build their careers. Millions of professional and aspiring programmers visit Heap Overflow each month to help solve coding problems, develop new skills.
                        </p>
                    </div>
        
                    <div className="col-md-3">     
                        <br/>
                        <div className="thumbnail">                            
                            <div className="caption">
                                <h4>Existing user :<br/><br/>
                                    
                                    <Link to="login" className="btn btn-primary btn-lg"> Login </Link>
                                </h4>                                
                            </div>
                        </div>
                    </div>

                    <div className="col-md-3"> 
                        <br/>
                        <div className="thumbnail">                            
                            <div className="caption">
                                <h4>New user :<br/><br/>
                                    
                                    <Link to="signup" className="btn btn-primary btn-lg"> Sign up </Link>
                                </h4>                                
                            </div>
                        </div>
                    </div>
            
                </div>                                       
			</div>  
        );
    }
}

export default HomePage;