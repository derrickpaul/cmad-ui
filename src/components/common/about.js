import React from 'react';

class About extends React.Component {
   render() {
      return (
			<div className="container">                    
              <div className="row">
                <div className="col-lg-12">
                    <h1 className="page-header">
                        <small>About</small>
                    </h1>
                 </div>
              </div>                    
              <div className="row">
                    <div className="col-md-6">
                        <img className="img-responsive" src="/logos/logo-large.png" alt="about us" height="450" width="750" />
                    </div>
                    <div className="col-md-6">
                        <h3>HeapOverflow</h3>
                        <p>This is a project developed as a part of CMAD co-hort initiative</p>
                        <p>The project is a cloud based application deployed on Google cloud platform and based on Docker containers</p>
                        <p>Technologies used comprises all the latest, greatest and hottest ones :)</p>
                        <p> <a href="https://bitbucket.org/derrickpaul/cmad-overview" target="_blank"> <button type="button" className="btn btn-success">Learn more</button></a></p>
                    </div>
                </div>
                         
                <div className="row">
                    <div className="col-lg-12">
                      <h1 className="page-header">
                        <small>Our Team</small>
                    </h1>

                    </div>
                    <div className="col-md-2 text-center">
                        <div className="thumbnail">
                            <img className="img-responsive" src="/img/nadevara.jpg" alt="" />
                            <div className="caption">
                                <h4>Nandan Devarajulu<br/>
                                    <small>CSG NSCG EPNM</small>
                                </h4>
                                <p>..</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2 text-center">
                        <div className="thumbnail">
                            <img className="img-responsive" src="/img/derpaul.jpg" alt="" />
                            <div className="caption">
                                <h4>Derrick Paul<br/>
                                    <small>SSF Delivery East-AS</small>
                                </h4>
                                <p>..</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2 text-center">
                        <div className="thumbnail">
                            <img className="img-responsive" src="/img/arunjoh.jpg" alt="" />
                            <div className="caption">
                                <h4>Arun John<br/>
                                    <small>FirePower-Dev India </small>
                                </h4>
                                <p>..</p>
                            </div>
                        </div>
                    </div>          
                </div>    
                                
			</div>
      );
   }
}

export default About;