import React from 'react';
import {Link} from 'react-router-dom';
import LoadingDots from './loadingDots';
import HeaderUser from './headerUser'
import Session from '../../store/session';

class Header extends React.Component {
    
    constructor(props,context){
        super(props,context);
        
        
        this.state = {            
            loggedInUser: {},
            userLoggedIn:false
        };

    }
    
   render() {
    //   debugger;
       
      
      let user ;
      let jwtToken = Session.getToken();
      if(jwtToken){
          user = Session.getUser();
      }
       
      return (
            <nav className="navbar navbar-default">        
                <div className="container-fluid">
                    <div className="navbar-header">
                      <button type="button" className="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar" aria-expanded="false">
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                      </button>
                      <a className="navbar-brand" href="#">                         
                        <img src="/logos/logo-m.png" width="60" className="responsive"/>                         
                      </a>        
                    </div>
                    <div className="collapse navbar-collapse" id="myNavbar">
                          <ul className="nav navbar-nav">
                            <li><Link className="active" to="/">Home</Link></li>
                            <li><Link to="/qlist">Questions</Link></li>
                            <li><Link to="/users">Users</Link></li>
                            <li><Link to="/about">About</Link></li>
                            <li><Link to="/chat">Chat</Link></li>
                          </ul>
                         <HeaderUser user={user}/>
                        {  this.props.loading && <LoadingDots interval={400} dots={20} />}
                    </div>
          
                </div>
            </nav>
      );
   }
}

export default Header;