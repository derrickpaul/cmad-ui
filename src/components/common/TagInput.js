import React from 'react';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';
import AjaxHelper from '../../api/ajaxHelper';

class TagInput extends React.Component {
  
  constructor(props, context) {
    super(props, context);

    this.state = {
      allowNew: true,
      multiple: true,
      options: [],
    };

    this.handleSearch = this.handleSearch.bind(this);
  }
  
  render() {
    return (
      <AsyncTypeahead
        {...this.state}
        clearButton
        onChange={this.props.onChange}
        labelKey="name"
        newSelectionPrefix="Add a new tag: "
        placeholder="Enter a tag or search for one..."
        onSearch={this.handleSearch}
      />
    );
  }

  handleSearch = query => {
    if (!query) {
      return;
    }

    fetch(AjaxHelper.getUrl() +  "questions/tags?tagName=" + query)
      .then(resp => resp.json())
      .then(json => this.setState({options: json.tags}));
  }
}

export default TagInput;