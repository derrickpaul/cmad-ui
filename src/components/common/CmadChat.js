
/**
 * Example usage of this library class:
 * var cmadChat = new CmadChat("ws://localhost:8084", "testuser1@cisco.com", 5);
 * 
 * // Add listener for chat messages.
 * cmadChat.addChatMessageListener(function(message){
 *   // Format of message: 
 *   {
 *      fromUserName: <EmailID>,
 *      toUserName: <EmailID>,
 *      message : <message>,
 *      type : "CHAT"
 *   }
 *   // Handle the received message, may be by populating the UI.
 * });
 * 
 * // Add listener for presence change events.
 * cmadChat.addPresenceMessageListener(function(message){
 *   // Format of message: 
 *   {
 *      userName: <EmailID>,
 *      presence: "OFFLINE" | "ONLINE",
 *      type: "USER_STATUS"
 *   }
 * });
 * 
 * // Add listener for error messages if required.
 * cmadChat.addErrorMessageListener(function(message){
 * 
 * });
 * // Initialize the chat web socket. 
 * cmadChat.connect();
 */
class CmadChat {
    constructor(_baseUrl, _userName, _retryCount = 60) {
        this.baseUrl = _baseUrl;
        this.userName = _userName;
        this.retryCount = _retryCount;
        
        this.onOpenCallback = null;
        this.onCloseCallback = null;
        this.onChatMessage = null;
        this.onPresenceMessage = null;
        this.onErrorCallback = null;
        
        this.socketOpen = false;
        this.socket = null;
        this.userDisconnected = false;
    }
    
    addOnOpenListener(callback) {
        this.onOpenCallback = callback;
    }
    
    removeOnOpenListener() {
        this.onOpenCallback = null;
    }
    
    addOnCloseListener(callback) {
        this.onCloseCallback = callback;
    }
    
    removeOnCloseListener() {
        this.onCloseCallback = null;
    }
    
    addChatMessageListener(callback) {
        this.onChatMessage = callback;
    }
    
    removeChatMessageListener() {
        this.onChatMessage = null;
    }
    
    addPresenceMessageListener(callback) {
        this.onPresenceMessage = callback;
    }
    
    removePresenceMessageListener() {
        this.onPresenceMessage = null;
    }
    
    addErrorMessageListener(callback) {
        this.onErrorCallback = callback;
    }
    
    removeErrorMessageListener() {
        this.onErrorCallback = null;
    }
    
    sendChat(toUserName, message) {
        if(this.socket == null)
            throw new Error("Chat library is not initialized. Please make sure you registered the callbacks and called the connect method.");
        
        if(this.socketOpen == false)
            return false;
        
        var data = {
            fromUserName : this.userName,
            toUserName : toUserName,
            message : message,
            type : "CHAT"
        };
        this.socket.send(JSON.stringify(data));
        
        return true;
    }
    
    askForPresence(userName) {
        if(this.socket == null)
            throw new Error("Chat library is not initialized. Please make sure you registered the callbacks and called the connect method.");
        
        if(this.socketOpen == false)
            return false;
        
        var data = {
            fromUserName : this.userName,
            toUserName : userName,
            message : "",
            type : "USER_STATUS"
        };
        this.socket.send(JSON.stringify(data));
            
        return true;
    }
    
    internalSocketOpen() {
        if(this.retryCount < 0) {
            console.log("Maximum retry count attempt reached.");
            return false;
        }
        
        this.socket = new WebSocket(this.baseUrl + "/apis/users/" + this.userName + "/chat");
        var that = this;
        
        this.socket.onmessage = function(event) {
            console.log("Message received: " + event.data);
            var message = JSON.parse(event.data);
            if(message.type == "CHAT" && that.onChatMessage != null) {
                that.onChatMessage(message);
            } else if (message.type == "USER_STATUS" && that.onPresenceMessage != null) {
                that.onPresenceMessage(message);
            } else {
                // Unknown message.
                message.error = "Unknown message";
                if(that.onErrorCallback) 
                    that.onErrorCallback(message);
            }
        }
        
        this.socket.onopen = function(event) {
            console.log("WebSocket opened. User is online.");
            that.socketOpen = true;
            if(that.onOpenCallback != null)
                that.onOpenCallback(event);
        };
        
        this.socket.onclose = function(event) {
            console.log("WebSocket closed. User is offline.");
            that.socketOpen = false;
            if(that.onCloseCallback != null)
                that.onCloseCallback(event);
            
            // Retry connection again after a sleep period.
            if(that.userDisconnected == false) {
                console.log("Attempting WebSocket connection again in 3 seconds.");
                setTimeout(function(){ that.internalSocketOpen() }, 3000);
            }
        };
        
        this.retryCount--;
        return true;
    }
    
    connect(retryAttempts) {
        this.userDisconnected = false;
        if(retryAttempts)
            this.retryCount = retryAttempts;
        
        return this.internalSocketOpen();
    }
    
    disconnect() {
        this.userDisconnected = true;
        this.socket.close();
    }
}

export default CmadChat;