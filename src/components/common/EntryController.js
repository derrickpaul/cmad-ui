import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Entry from './Entry';
import * as types from './entryTypes';

import * as questionActions from '../../actions/questionActions';

class EntryController extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onVoteUp = this.onVoteUp.bind(this);
        this.onVoteDown = this.onVoteDown.bind(this);
        this.onSaveText = this.onSaveText.bind(this);
        this.onSaveTags = this.onSaveTags.bind(this);
        this.onSaveComment = this.onSaveComment.bind(this);
    }

    onVoteUp() {
        var type = this.props.type;
        var entryId = this.props.entry._id;
        if (this.props.type === types.ANSWER) {
            entryId = this.props.entry.answerId;
        }

        if (this.props.type == types.QUESTION) {
            this.props.actions.voteUpQuestion(entryId);
        } else {
            var questionId = this.props.questionId;
            this.props.actions.voteUpAnswer(questionId, entryId);
        }
    }

    onVoteDown() {
        var type = this.props.type;
        var entryId = this.props.entry._id;
        if (this.props.type === types.ANSWER) {
            entryId = this.props.entry.answerId;
        }

        if (this.props.type === types.QUESTION) {
            this.props.actions.voteDownQuestion(entryId);
        } else {
            var questionId = this.props.questionId;
            this.props.actions.voteDownAnswer(questionId, entryId);
        }
    }

    onSaveText(text) {
        var type = this.props.type;
        var entryId = this.props.entry._id;
        if (this.props.type === types.ANSWER) {
            entryId = this.props.entry.answerId;
        }

        if (this.props.type === types.QUESTION) {
            this.props.actions.editQuestionText(entryId, text);
        } else {
            var questionId = this.props.questionId;
            this.props.actions.editAnswerText(entryId, questionId, text);
        }
    }

    onSaveTags() {
        // TODO call edit question/answer tags.
    }

    onSaveComment(text) {
        var type = this.props.type;
        var entryId = this.props.entry._id;
        if (this.props.type === types.ANSWER) {
            entryId = this.props.entry.answerId;
        }

        if (this.props.type === types.QUESTION) {
            this.props.actions.addCommentForQuestion(entryId, text);
        } else {
            var questionId = this.props.questionId;
            this.props.actions.addCommentForAnswer(questionId, entryId, text);
        }
    }

    render() {
        return(
            <Entry 
            entry={this.props.entry} 
            onSaveText={this.onSaveText} 
            onVoteUp={this.onVoteUp} 
            onVoteDown={this.onVoteDown} 
            onSaveText={this.onSaveText} 
            onSaveComment={this.onSaveComment}
            type={this.props.type}/>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions : bindActionCreators(questionActions, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(EntryController);