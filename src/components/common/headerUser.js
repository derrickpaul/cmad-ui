import React from 'react';
import {Link} from 'react-router-dom';
import Session from '../../store/session';

class HeaderUser extends React.Component {
    
   render() {       
      var user =this.props.user;   
       
      if(user){          
          return (          
                <ul className="nav navbar-nav navbar-right">
                  <li> <a className="navbar-brand" href="#"><small>Welcome {user.firstName}</small></a> </li>
                  <li> <Link className="glyphicon glyphicon-log-out" to="/logout"> Logout </Link></li>
                </ul>               
          );
          
      } else {
          return (          
                <ul className="nav navbar-nav navbar-right">
                  <li><Link className="glyphicon glyphicon-user" to="/signup"> Signup</Link></li>
                  <li><Link className="glyphicon glyphicon-log-in" to="/login"> Login</Link></li>
                </ul>               
          );
          
      }
       
   }
}

export default HeaderUser;