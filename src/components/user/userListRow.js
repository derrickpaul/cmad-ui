import React from 'react';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';

const UserListRow = ({user}) => {
    
    return(
                    <div className="col-md-3 text-center">
                        <div className="thumbnail">
                            <img className="img-responsive" src="/img/user.jpg" alt="" />
                            <div className="caption">
                                <h6><b>{user.firstName} {user.lastName} </b> <br/> </h6>
                                <p><span className="glyphicon glyphicon-envelope"></span> {user.userName}</p>
                                <p></p>
                            </div>
                        </div>
                    </div>          
  );  
};

UserListRow.propTypes = {
    user: PropTypes.object.isRequired
}

export default UserListRow;