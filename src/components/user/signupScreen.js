import React from 'react';
import TextInput from '../common/textInput';
import PasswordInput from '../common/passwordInput';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';


class SignupScreen extends React.Component {
   render() {
       debugger;
      const user = this.props.user? this.props.user :{};       
       
      return (
            <div className="container">
            <div id="login-overlay" className="modal-dialog">
              <div className="modal-content">
                  <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">×</span><span className="sr-only">Close</span></button>
                      <h4 className="modal-title" id="myModalLabel">Sign Up</h4>
                      
                        <div className="modal-body">
                            <div className="row">          
                                <div className="col-md-6"> 
                                   <img src="/logos/logo-m.png" width="225" className="img-responsive" />                         
                                    <hr/>
                                    <div> <h5>Welcome to world of Knowledge</h5> </div>
                                </div>          							
                                <div className="col-md-6">
                                    <div className="well">
                                        <form id="loginForm" method="POST" action="/login/"  className="form-horizontal" >
                                            <TextInput
                                                name="userName"
                                                label="User Name: [Email ID]"
                                                value={user.userName}
                                                error={this.props.errors.userName}
                                                disabled = {user.userName? true: false}
                                                placeholder="user@gmail.com"
                                                onChange={this.props.onChange}/>          
                                            <TextInput
                                                name="firstName"
                                                label="First name:"
                                                value={user.firstName}
                                                error={this.props.errors.firstName}
                                                placeholder="Chuck"
                                                onChange={this.props.onChange}/>
                                            <TextInput
                                                name="lastName"
                                                label="Last name:"
                                                value={user.lastName}
                                                error={this.props.errors.lastName}
                                                placeholder="Robbins"
                                                onChange={this.props.onChange}/>
                                            <PasswordInput
                                                name="password"
                                                label="Password:"
                                                value={user.password}                                                
                                                error={this.props.errors.password}                                                
                                                onChange={this.props.onChange}/>

                                          <PasswordInput
                                                name="password2"
                                                label="Re-enter Password:"
                                                value={user.password2}                                                
                                                error={this.props.errors.password2}                                                
                                                onChange={this.props.onChange}/>

                                            <input 
                                                type="submit"
                                               // disabled={this.props.saving}
                                                //value={ this.props.saving ? 'Saving...': 'Save'}
                                                value= 'Save'
                                                className="btn btn-primary"
                                                onClick = {this.props.onSave} />
                                                &nbsp;
                                                &nbsp;
                                            
                                            <Link className="btn btn-primary"
                                                role="button"
                                                to="/" >Cancel</Link>
          
                                        </form>
                                    </div>
                                </div>
                             </div>
                         </div>
                    </div>
               </div>
            </div>  
            </div>
      );
   }
}

SignupScreen.PropTypes = {
    user: PropTypes.object.isRequired,
    onSave: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    saving: PropTypes.bool,
    errors: PropTypes.object
}

export default SignupScreen;