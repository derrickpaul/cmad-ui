import React from 'react';
import TextInput from '../common/textInput';
import PropTypes from 'prop-types'


const UserForm = ({user, onSave, onChange, saving, errors}) => {
    return(
        <form>
            <h1> Manage user </h1>
            <TextInput
                name="firstName"
                label="First name:"
                value={user.firstName}
                errors={errors.title}
                placeholder="Enter first name"
                onChange={onChange}/>
            <TextInput
                name="lastName"
                label="Last name:"
                value={user.lastName}
                errors={errors.title}
                placeholder="Enter last name"
                onChange={onChange}/>
            <TextInput
                name="userName"
                label="User Name:"
                value={user.userName}
                errors={errors.title}
                disabled = {user.userName? true: false}
                placeholder="Enter user name"
                onChange={onChange}/>

            <input 
                type="submit"
                disabled={saving}
                value={ saving ? 'Saving...': 'Save'}
                className="btn btn-primary"
                onClick = {onSave} />
        </form>
    );
};

UserForm.PropTypes = {
    user: PropTypes.object.isRequired,
    onSave: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    saving: PropTypes.bool,
    errors: PropTypes.object
}

export default UserForm;