import React from 'react';
import Session from '../../store/session';
import {Link} from 'react-router-dom';


class LogoutScreen extends React.Component {
    
    render(){
        var user = Session.getUser() ;
        //clear the session
        localStorage.clear();
        
        debugger;
        return (
            <div className="container">
            <div className="jumbotron">
                <h3> Goodbye {user.firstName} ! </h3>
                <Link to="home" className="btn btn-primary btn-lg"> Home </Link>
            </div>
            </div>            
        );
    } 
}

export default LogoutScreen