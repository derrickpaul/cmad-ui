import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types'
import * as userActions from '../../actions/userActions';
import UserForm from './userForm';
import { parse } from 'query-string';
import toastr from 'toastr';
import LoginScreen from './loginScreen';
import $ from 'jquery';

class ManageUserLogin extends React.Component {
    constructor(props,context){
        super(props,context);
        
        this.state = {
            loggedInUser: { jwtToken:"", validUser:false},
            errors :{},
            saving:true
        };
        this.updateFormState = this.updateFormState.bind(this);
        this.submitLogin = this.submitLogin.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
    } 
    
    updateFormState(event){
      //  debugger;
        const field  = event.target.name;
        var errors = this.state.errors;
        let user = Object.assign({},this.state.loggedInUser);
        user[field]=event.target.value;
        if (errors[field]){
           errors[field] ="";
        }
        return this.setState({loggedInUser:user, errors: errors});
    }
    

    submitLogin(event){
        event.preventDefault();
        var validationErrors = this.validateForm();
        
        if($.isEmptyObject(validationErrors)){            
            this.setState({saving:true});
            debugger;
            this.props.actions.loginUser(this.state.loggedInUser)    
                .then(() => this.redirectToHomePage())
                .catch( error => {
                    this.setState({saving:false});
                    toastr.error(error);
                } );
            //debugger;
            //this.context.router.path('/users');
            
        } else {
            this.setState(Object.assign({},this.state,{errors:validationErrors},{saving:true}))
        }
    }
    
    validateForm(){
        var errors = {};
        var user = this.state.loggedInUser;
        //debugger;
        if(!user.userName){        
            errors.userName = 'User name is mandatory'
        } else if (!this.validateEmail(user.userName)){
            errors.userName = 'Invalid email ID'
        }
        
        if(!user.password){
            errors.password = 'Password is mandatory'
        }else if (user.password.length < 5 ){
            errors.password = 'Password too short'
        }
        return errors;
    }
    
    validateEmail(mail)   {  
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  ? true : false ;      
    }  

    redirect(){
      this.setState({saving:false});
      toastr.success('User saved');
      this.props.history.push('/users')
    }
    
    redirectToHomePage(){
        //debugger;
      this.setState({saving:false});
      toastr.success('Login Success');
      this.props.history.push('/')
    }    
    
    render(){
        //debugger;
            return(
                    <LoginScreen    
                        loggedInUser={this.state.loggedInUser}
                        onChange={this.updateFormState}
                        onSubmit={this.submitLogin}            
                        errors={this.state.errors}
                        saving={this.state.saving}
                     />
            );
            
        
    }
}


ManageUserLogin.contextTypes = {
    router :PropTypes.object
}

function getUserById(users, id){
    debugger;
    const filteredUsers = users.filter( user => user.id === id);
    if(filteredUsers) return filteredUsers[0] ;
    return null;
}

function mapStateToProps(state,ownProps) {
    debugger;
    
    return {
        user: state.loggedInUser
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(ManageUserLogin);