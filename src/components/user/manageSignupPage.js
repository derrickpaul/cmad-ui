import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types'
import * as userActions from '../../actions/userActions';
import UserForm from './userForm';
import SignupScreen from './signupScreen';
import { parse } from 'query-string';
import toastr from 'toastr';
import $ from 'jquery';

class ManageSignupPage extends React.Component {
    constructor(props,context){
        super(props,context);
        
        this.state = {            
            newUser: {},
            errors :{},
            saving:false
        };
        this.updateNewUserState = this.updateNewUserState.bind(this);
        this.submitSignup = this.submitSignup.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.validateEmail = this.validateEmail.bind(this);

    } 
    
    componentDidMount() {
        debugger;
        this.props.actions.loadUserById(this.props.match.params.id);
    }
    
    
    updateNewUserState(event){        
        debugger;
        var errors = this.state.errors;
        const field  = event.target.name;
        let user = Object.assign({},this.state.newUser);
        user[field]=event.target.value;
        if (errors[field]){
           errors[field] ="";
        }                
        return this.setState({newUser:user,errors: errors});
    }    
    


    submitSignup(event){
        event.preventDefault();                
        var validationErrors = this.validateForm();
        
        debugger;
        if($.isEmptyObject(validationErrors)){  
            this.setState({saving:true});
                this.props.actions.createUser(this.state.newUser)    
                .then(() => this.redirectToHomePage())
                .catch( error => {
                    //this.setState({saving:false});
                    toastr.error(error);
                });            
            
        } else {
                this.setState(Object.assign({},this.state,{errors:validationErrors},{saving:true}))

        }

    }
    
    redirectToHomePage(){
        debugger;
      this.setState({saving:false});
      toastr.success('User creation Success');
      this.props.history.push('/')
    }    

    validateForm(){
        var errors = {};
        var user = this.state.newUser;
        debugger;
        //this.context.router.path('/users');
        if(!user.userName){        
            errors.userName = 'User name is mandatory'
        } else if (!this.validateEmail(user.userName)){
            errors.userName = 'Invalid email ID'
        }
        
        if(!user.password){
            errors.password = 'Password is mandatory'
        }else if (user.password.length < 5 ){
            errors.password = 'Password too short'
        }else if ( user.password != user.password2){
            errors.password = 'Passwords dont match'
        }
        
        if(!user.firstName){
            errors.firstName = 'First name is mandatory'
        }else if (user.firstName.length < 3 ){
            errors.firstName = 'First name too short'
        }
        
        if(!user.lastName){
            errors.lastName = 'Last name is mandatory'
        }else if (user.lastName.length < 3 ){
            errors.lastName = 'Last name too short'
        }
        return errors;
    }
    
    validateEmail(mail)   {  
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  ? true : false ;      
    }  
    
    render(){
        debugger;

        return(
            <SignupScreen 
                user={this.props.newUser}                     
                onChange={this.updateNewUserState}
                onSave={this.submitSignup}            
                errors={this.state.errors}
                saving={this.state.saving}
             />
        );        
    }
}

ManageSignupPage.propTypes = {
    user : PropTypes.object.isRequired,
    actions : PropTypes.object.isRequired
}


ManageSignupPage.contextTypes = {
    router :PropTypes.object
}

function getUserById(users, id){
    debugger;
    const filteredUsers = users.filter( user => user.id === id);
    if(filteredUsers) return filteredUsers[0] ;
    return null;
}

function mapStateToProps(state,ownProps) {
    debugger;
    
    return {
        newUser: state.newUser
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(ManageSignupPage);