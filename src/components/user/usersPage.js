import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types'
import * as usersActions from '../../actions/userActions';
import {bindActionCreators} from 'redux';
import UserList from './userList';
//import {browserHistory} from 'react-router';


class UsersPage extends React.Component {
    constructor(props,context){
        super(props,context);
        this.redirectToAddUserPage = this.redirectToAddUserPage.bind(this);
    }
    
    componentDidMount() {
        this.props.actions.loadUsers();
    }

    userRow(user,index) {
        return <div key={index}>{user.firstName} </div>
    }
    
    redirectToAddUserPage(){
        debugger;

        this.props.history.push('/user')
    }
    
    render(){
        
        const {users} = this.props;        
        //debugger;
        return (
            <div className="container">

                <UserList users={users} />                
            </div>
        );
    } 
}

UsersPage.propTypes = {
    users: PropTypes.array.isRequired,
    actions : PropTypes.object.isRequired
}

function mapStateToProps(state,ownProps) {
    debugger;
    return {
      users:state.users  
    };
}

function mapDispatchToProps(dispatch){
    return {
        //createUser : user => dispatch(usersActions.createUser(user))
        actions : bindActionCreators(usersActions, dispatch)
    };
}

const connectedStateAndProps = connect(mapStateToProps,mapDispatchToProps);
export default connectedStateAndProps(UsersPage);
