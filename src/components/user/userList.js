import React from 'react';
import PropTypes from 'prop-types'
import UserListRow from './userListRow';



const UserList = ({users})=>{
    
   // debugger;    
        
    return (
          <div>
          <div className="row">
            <div className="col-lg-12">
                <h1 className="page-header">
                    <small>Current Users</small>
                </h1>
             </div>
          </div> 
          <div className="row">
            {
                users.map(user => 
                <UserListRow key={user._id} user={user} />)
            }        
          </div>                
          </div>
    );
};

UserList.propTypes = {
    users:PropTypes.array.isRequired
}

export default UserList;