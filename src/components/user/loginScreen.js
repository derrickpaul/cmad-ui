import React from 'react';
import TextInput from '../common/textInput';
import PasswordInput from '../common/passwordInput';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';

class LoginScreen extends React.Component {
   render() {
       debugger;
      const user = this.props.loggedInUser;       
       
      return (
            <div className="container">
            <div id="login-overlay" className="modal-dialog">
              <div className="modal-content">
                  <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">×</span><span className="sr-only">Close</span></button>
                      <h4 className="modal-title" id="myModalLabel">Login to HeapOverflow</h4>
                        <div className="modal-body">
                            <div className="row">          
                                <div className="col-md-6">
                                    <div className="well">
                                        <form id="loginForm" method="POST"  noValidate="noValidate">
                                            <TextInput
                                                name="userName"
                                                label="User Name:"
                                                value={user.userName}
                                                error={this.props.errors.userName}
                                                disabled = ""
                                                placeholder="user@gmail.com"
                                                onChange={this.props.onChange}/>          
          
                                            <PasswordInput
                                                name="password"
                                                label="Password:"
                                                value={user.password}
                                                error={this.props.errors.password}
                                                onChange={this.props.onChange}/>
          
                                          <div id="loginErrorMsg" className="alert alert-error hide">Wrong username og password</div>          
                                          <button type="submit" className="btn btn-success btn-block" onClick = {this.props.onSubmit}>Login</button>          
                                        </form>
                                    </div>
                                </div>
                                <div className="col-md-6">          
                                        <p className="lead">Register now for <span className="text-success">FREE</span></p>
                                            <ul className="list-unstyled" >
                                              <li><span className="fa fa-check text-success"></span> 1,23,900 Total users</li>
                                              <li><span className="fa fa-check text-success"></span> 5000+ Active users</li>
                                              <li><span className="fa fa-check text-success"></span> Get all your queries answered</li>
                                              <li><span className="fa fa-check text-success"></span> Expert advise (paid service)</li>                                              
                                              <li><span className="fa fa-check text-success"></span> Hire experts on demand ...</li>                                              
                                              <li><span className="fa fa-check text-success"></span> Many more...</li>                                              
                                          </ul>                                        
                                        <p><Link className="btn btn-info btn-block"    role="button" to="/signup" >Yes please, register now!</Link></p>
                                </div>          
                             </div>
                         </div>
                    </div>
               </div>
            </div> 
            </div>
      
      );
   }
}

LoginScreen.PropTypes = {
    loggedInUser: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    saving: PropTypes.bool,
    errors: PropTypes.object
}
export default LoginScreen;