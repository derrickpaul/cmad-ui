import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types'
import * as userActions from '../../actions/userActions';
import UserForm from './userForm';
import SignupScreen from './signupScreen';
import { parse } from 'query-string';
import toastr from 'toastr';

class ManageUserPage extends React.Component {
    constructor(props,context){
        super(props,context);
        
        this.state = {
            user: Object.assign({},this.props.user),
            loggedInUser: {},
            errors :{},
            saving:false
        };
        this.updateUserState = this.updateUserState.bind(this);
        this.saveUser = this.saveUser.bind(this);
    } 
    
    componentDidMount() {
        debugger;
        this.props.actions.loadUserById(this.props.match.params.id);
    }
    
    
    updateUserState(event){
        debugger;
        const field  = event.target.name;
        let user = Object.assign({},this.state.user);
        user[field]=event.target.value;
        return this.setState({user:user});
    }
    

    saveUser(event){
        event.preventDefault();
        this.setState({saving:true});
        this.props.actions.saveUser(this.state.user)    
            .then(() => this.redirect())
            .catch( error => {
                this.setState({saving:false});
                toastr.error(error);
            } );
        //debugger;
        //this.context.router.path('/users');
    }
    
    
    redirect(){
      this.setState({saving:false});
      toastr.success('User saved');
      this.props.history.push('/users')
    }
    
    render(){
        debugger;
            return(
                    <UserForm 
                        user={this.props.user}                     
                        onChange={this.updateUserState}
                        onSave={this.saveUser}            
                        errors={this.state.errors}
                        saving={this.state.saving}
                     />
            );
    
    }
}

ManageUserPage.propTypes = {
    user : PropTypes.object.isRequired,
    actions : PropTypes.object.isRequired
}


ManageUserPage.contextTypes = {
    router :PropTypes.object
}

function getUserById(users, id){
    debugger;
    const filteredUsers = users.filter( user => user.id === id);
    if(filteredUsers) return filteredUsers[0] ;
    return null;
}

function mapStateToProps(state,ownProps) {
    debugger;
    
    return {
        user: state.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(ManageUserPage);