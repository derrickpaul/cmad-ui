import React , { Component } from 'react';
import PropTypes from 'prop-types'

import {
  // NOTE no more browserHistory https://reacttraining.com/react-router/web/api/BrowserRouter
  BrowserRouter as Router,
  Route  
} from 'react-router-dom';

  
import Header from './components/common/header';
import Home from './components/common/homePage';
import About from './components/common/about';
import QuestionsPage from './components/questions/QuestionsPage';
import QuestionContoller from './components/questions/QuestionController';
import LoginScreen from './components/user/manageUserLogin';
import LogoutScreen from './components/user/logoutScreen';
import UsersPage from './components/user/usersPage' ;
import ManageUserPage from './components/user/manageUserPage' ;
import ManageSignupPage from './components/user/manageSignupPage' ;
import AskQuestionPage from './components/questions/AskQuestionPage';
import ChatController from './components/common/ChatController';
import {connect} from 'react-redux';
import toastr from 'toastr';


class App extends Component {

   toCamelCase(str) {
        var sentence = str.toLowerCase()
            .replace(/_+(\w|$)/g, function ($$, $1) {
                return " " + $1;
            });
        return sentence.charAt(0).toUpperCase() + sentence.slice(1);
    }
  
  componentWillReceiveProps(newProps) {
        var action = newProps.status.action;
        var error = newProps.status.error;
        var message = this.toCamelCase(action);
        toastr.options.timeOut = 1000;
        if (action.indexOf('ERROR') !== -1) {
            toastr.error(error, {timeOut: 3000})
        } else if (action.indexOf('SUCCESS') !== -1) {
            toastr.success(message, {timeOut: 3000});
        } else {
            toastr.info(message, {timeOut: 3000});
        }
  }

  render() {
    return (
      <Router>    
          <div >
            <Header
                loading={this.props.loading}
            />
             <Route exact path="/" component={Home}/>
             <Route path="/login" component={LoginScreen}/>
             <Route path="/logout" component={LogoutScreen}/>
             
        
             <Route path="/qlist" component={QuestionsPage}/>
             <Route path="/questions/:questionId" component={QuestionContoller}/>
             <Route path="/ask" component={AskQuestionPage}/>
        
             <Route path="/users" component={UsersPage}/>
             <Route path="/user/:id" component={ManageUserPage}/>

             <Route path="/chat" component={ChatController}/>

             <Route path="/signup" component={ManageSignupPage} />        

             <Route path="/about" component={About}/>

          </div>
      </Router>    
    );
  }
}

App.propTypes = {
    children: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired
}

function mapStateToProps(state,ownProps){
    return {
        loading: state.ajaxCallsInProgress > 0,
        status: state.ajaxStatus
    }
}

export default connect(mapStateToProps)(App);
