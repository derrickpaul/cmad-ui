export const InitialState = {
    total: 0,
    data: [],
    question: {
        _id: "null",
        answers: [],
        tags: [],
        userId: "",
        text: "",
        comments: []
    },
    users: [],
    user:{},
    loggedInUser:{},
    ajaxCallsInProgress: 0,
    ajaxStatus: {
        action: null,
        error: ""
    },
    answer: "",
}