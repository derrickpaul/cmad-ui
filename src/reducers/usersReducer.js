import * as types from '../actions/actionTypes';
import * as initState from './initialState';

export default function userReducers(state = initState.InitialState.users ,action){

    switch(action.type) {
        
        case types.LOAD_USERS_SUCCESS :       
            // debugger;
            
            Object.assign({},action.user)
            
            var result = [];
            if(action && action.users && action.users.data)
                result = action.users.data;
            return result;
            
        default:
           // debugger;
            return state;            
    }
} 