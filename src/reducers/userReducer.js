import * as types from '../actions/actionTypes';
import * as initState from './initialState';

export default function userReducer(state = initState.InitialState.user ,action){

    switch(action.type) {
              
        case types.CREATE_USER_SUCCESS :  
            //debugger;
            return [
                ...state.users,
                Object.assign({},action.user)
            ];
            
        case types.UPDATE_USER_SUCCESS :  
            //debugger;
            return [
                ...state.users.filter(user => user.id !== action.user.id),
                Object.assign({},action.user)
            ];
            
        case types.LOAD_USER_SUCCESS:
            // debugger;
            return Object.assign({},state,action.user);
            
        default:
           // debugger;
            return state;            
    }
} 