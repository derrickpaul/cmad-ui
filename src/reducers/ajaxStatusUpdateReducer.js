import * as types from '../actions/actionTypes';
import * as initState from './initialState';

function actionTypeEndsInSuccess(type) {
    return type.substring(type.length - 8) == '_SUCCESS';
}

export default function ajaxStatusUpdateReducer(state = initState.InitialState.ajaxStatus, action) {

    return {
        action: action.type,
        error: action.error
    };
}