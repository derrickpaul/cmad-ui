import {combineReducers} from 'redux';
import user from './userReducer';
import users from './usersReducer';
import questions from './questionReducer';
import login from './loginReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';
import ajaxStatus from './ajaxStatusUpdateReducer';

const rootReducer = combineReducers({
    user,
    users,
    login,
    questions,
    ajaxCallsInProgress,
    ajaxStatus
});

export default rootReducer;