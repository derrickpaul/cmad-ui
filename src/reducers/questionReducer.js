import * as types from '../actions/actionTypes';
import * as initialState from './initialState';

export default function questionReducer(state = initialState.InitialState, action) {
    var newState = copyDeep(state);
    switch(action.type) {
        case types.LOAD_QUESTIONS_SUCCESS:
            newState.data = copyDeep(action.questions.data);
            newState.total = copyDeep(action.questions.total);
            return newState;
        case types.LOAD_QUESTION_SUCCESS:
            newState.question = copyDeep(action.question);
            return newState;
        case types.CREATE_QUESTION_SUCCESS:
            newState.questionPath = action.questionPath;
            return newState; 
        default:
            return state;
    }
}

function copyDeep(obj) {
    return JSON.parse(JSON.stringify(obj));
}