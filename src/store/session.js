class Session {
    
    static JWT_TOKEN = 'JWT_TOKEN';
    static CURRENT_USER = 'CURRENT_USER';

    static storeToken(token) {
		if(typeof(Storage) !== undefined ){
            localStorage.setItem(this.JWT_TOKEN,token);

        } else {
            alert('local storage not supported');
        }
    }

    static getToken() {
        var result;        
		if(typeof(Storage) !== undefined ){
            result = localStorage.getItem(this.JWT_TOKEN);
        } else {
            alert('local storage not supported');
        }
        return result;
    }

    static storeUser(user) {
		if(typeof(Storage) !== undefined ){        
            localStorage.setItem(this.CURRENT_USER,JSON.stringify(user));
        } else {
            alert('local storage not supported');
        }
    }

    static getUser() {
        var result = {};        
		if(typeof(Storage) !== undefined ){
            var resultString = localStorage.getItem(this.CURRENT_USER);
            result = JSON.parse(resultString);
        } else {
            alert('local storage not supported');
        }
        return result;
    }

    static set(key, value) {
		if(typeof(Storage) !== undefined ){
            localStorage.setItem(key,value);

        } else {
            alert('local storage not supported');
        }
    }

    static get(key) {
        var result;        
		if(typeof(Storage) !== undefined ){
            result = localStorage.getItem(key);
        } else {
            alert('local storage not supported');
        }
        return result;
    }
} 

export default Session;