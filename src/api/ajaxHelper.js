class AjaxHelper {
    
    static DEFAULT_API_URL = "http://localhost:8084/apis/";
    static DEFAULT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkZXJwYXVsQGNpc2NvLmNvbSIsImZpcnN0TmFtZSI6IkRlcnJpY2siLCJsYXN0TmFtZSI6IlBhdWwiLCJpYXQiOjE0OTk3NTM5ODV9.m8YGrRUOHmVFC9xDn8AwnLAYMA9nRFedbA5srbP-tlo=";

     static getUrl() {
         var defaultUrl = "http://localhost:8082/apis/";

        if (window.location.hostname == "cmad-ui.learnitall.club" || window.location.hostname == "cmad-apis.learnitall.club")
                    defaultUrl = "http://cmad-apis.learnitall.club/apis/";
                else if (window.location.hostname == "cmad-ui.derrickpaul.in" || window.location.hostname == "cmad-apis.derrickpaul.in")
                    defaultUrl = "http://cmad-apis.derrickpaul.in/apis/";

        return defaultUrl;        
     }

     static getUsersUrl() {
         var defaultUrl = "http://localhost:8084/apis/";

        if (window.location.hostname == "cmad-ui.learnitall.club" || window.location.hostname == "cmad-apis.learnitall.club")
                    defaultUrl = "http://cmad-apis.learnitall.club/apis/";
                else if (window.location.hostname == "cmad-ui.derrickpaul.in" || window.location.hostname == "cmad-apis.derrickpaul.in")
                    defaultUrl = "http://cmad-apis.derrickpaul.in/apis/";

        return defaultUrl;        
     }

      static getChatUrl() {
         var defaultUrl = "ws://localhost:8084";

        if (window.location.hostname == "cmad-ui.learnitall.club" || window.location.hostname == "cmad-apis.learnitall.club")
                    defaultUrl = "ws://cmad-apis.learnitall.club";
                else if (window.location.hostname == "cmad-ui.derrickpaul.in" || window.location.hostname == "cmad-apis.derrickpaul.in")
                    defaultUrl = "ws://cmad-apis.derrickpaul.in";

        return defaultUrl;        
     }

     static get(url, token=this.DEFAULT_TOKEN) {
        return new Promise((resolve, reject) => {
            var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
            const reqUrl = AjaxHelper.getUsersUrl() + url;
            xmlhttp.open("GET", reqUrl, true);
            xmlhttp.setRequestHeader("Authorization", 'bearer ' + token);
            xmlhttp.setRequestHeader("Accept","application/json");
            xmlhttp.send();

            xmlhttp.onreadystatechange=function(){
            if(xmlhttp.readyState===4)
            {
                if(xmlhttp.status===200 || xmlhttp.status===201)
                {
                    var response=JSON.parse(xmlhttp.responseText);
                    resolve(response);
                } else {
                    reject(xmlhttp.statusText);
                }
            }
            else    console.log("Response recieved with status "+xmlhttp.status);
        }});
    }      

    static loginPost(url, data, type='POST') {
        return new Promise((resolve, reject) => {
            var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
            const reqUrl = AjaxHelper.getUsersUrl() + url;
            xmlhttp.open(type, reqUrl, true);
            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");            
            xmlhttp.setRequestHeader("Accept","application/json");
            if ('undefined' !== typeof data) {
                xmlhttp.send(JSON.stringify(data))
            } else {
                xmlhttp.send();
            }

            xmlhttp.onreadystatechange=function(){
            if(xmlhttp.readyState===4)
            {
                if(xmlhttp.status===200 || xmlhttp.status===201)
                {
                    debugger;                    
                    var response = xmlhttp.response;                    
                    var responseJson = response? JSON.parse(response) : {};                    
                    resolve(responseJson);
                } else {
                    
                    var response = {}
                    response.status = xmlhttp.status;
                    response.statusText = xmlhttp.statusText;
                    
                    if(response.status == 401 || response.status > 500 || response.status == 409 ||  response.status == 422){
                        debugger;
                        reject(response);
                    }
                        
                }
            }
            else    console.log("Response recieved with status "+xmlhttp.status);
        }});
    }

    static post(url, data, type='POST') {
        return new Promise((resolve, reject) => {
            var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
            const reqUrl = AjaxHelper.getUsersUrl() + url;
            xmlhttp.open(type, reqUrl, true);
            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");    
            xmlhttp.setRequestHeader("Authorization", 'bearer ' + 'token');            
            xmlhttp.setRequestHeader("Accept","application/json");
            if ('undefined' !== typeof data) {
                xmlhttp.send(JSON.stringify(data))
            } else {
                xmlhttp.send();
            }

            xmlhttp.onreadystatechange=function(){
            if(xmlhttp.readyState===4)
            {
                if(xmlhttp.status===200 || xmlhttp.status===201)
                {
                    var response = xmlhttp.response;                    
                    var responseJson = JSON.parse(response);
                    debugger;
                    resolve(responseJson);
                } else {
                    
                    var response = {}
                    response.status = xmlhttp.status;
                    response.statusText = xmlhttp.statusText;
                    
                    if(response.status == 401 || response.status > 500 || response.status == 409 ||  response.status == 422){
                        debugger;
                        reject(response);
                    }
                        
                }
            }
            else    console.log("Response recieved with status "+xmlhttp.status);
        }});
    }
    
} 

export default AjaxHelper;