import delay from './delay';
import $ from 'jquery';
import AjaxHelper from './ajaxHelper';

const users = [];


class UserApi {
    
   static getAllUsers() {      
    return AjaxHelper.get('users') ;      
   }

  static getUserById(userId) {
        debugger;
       return AjaxHelper.get('users/' + userId);
  }    
    
  static loginUser(user) {

      debugger;
        return AjaxHelper.loginPost('tokens',user) ;      
  }
    
  static createUser(user) {

      debugger;
        return AjaxHelper.loginPost('users',user) ;      
  }    

      
   static saveUser(user) {
	user = Object.assign({}, user); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minUserNameLength = 3;
        if (user.firstName.length < minUserNameLength) {
          reject(`First Name must be at least ${minUserNameLength} characters.`);
        }

        if (user.lastName.length < minUserNameLength) {
          reject(`Last Name must be at least ${minUserNameLength} characters.`);
        }

        if (user.id) {
          const existingUserIndex = users.findIndex(a => a.id === user.id);
          users.splice(existingUserIndex, 1, user);
        } else {
          //Just simulating creation here.
          //The server would generate ids for new users in a real app.
          //Cloning so copy returned is passed by value rather than by reference.
          //user.id = generateId(user);
           user.id = 'generateId(user)';
          users.push(user);
        }

        resolve(user);
      }, delay);
    });
  }

  static deleteUser(userId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfUserToDelete = users.findIndex(user => {
          user.id == userId;
        });
        users.splice(indexOfUserToDelete, 1);
        resolve();
      }, delay);
    });
  }
    

}

export default UserApi;