import delay from './delay';
import $ from 'jquery';
import Session from '../store/session';
import AjaxHelper from './ajaxHelper';

const questions = [];

export function getNewestQuestions(skip, limit) {
    return get(defaultUrl + 'questions?sortKey=lastUpdatedTime&' + appendSearchParam(skip, limit));
}

export function getMostVotedQuestions(skip, limit) {
    return get(defaultUrl + 'questions?sortKey=voteCount&' + appendSearchParam(skip, limit));
}

export function getMostViewedQuestions(skip, limit) {
    return get(defaultUrl + 'questions?sortKey=viewCount&' + appendSearchParam(skip, limit));
}

function appendSearchParam(skip, limit) {
    return 'skip=' + ('undefined' === typeof skip  ? 0 : skip) + '&limit=' +  ('undefined' === typeof limit  ? 3 : limit);
}

export function getQuestionById(questionId) {
   return get(defaultUrl + 'questions/' + questionId);
}

export function voteUpQuestion(questionId) {
    return authorizedWrite(defaultUrl + 'questions/' + questionId + '?' + 'action=VOTE_UP', '', 'PUT');
}

export function voteDownQuestion(questionId) {
    return authorizedWrite(defaultUrl + 'questions/' + questionId + '?' + 'action=VOTE_DOWN', '', 'PUT');
}

export function voteUpAnswer(answerId) {
    return authorizedWrite(defaultUrl + 'questions/answers/' + answerId + '?' + 'action=VOTE_UP', '', 'PUT');
}

export function voteDownAnswer(answerId) {
    return authorizedWrite(defaultUrl + 'questions/answers/' + answerId + '?' + 'action=VOTE_DOWN', '', 'PUT');
}

export function editQuestionText(questionId, text) {
    return authorizedWrite(defaultUrl + 'questions/' + questionId + '?' + 'action=CONTENT_UPDATE', {text}, 'PUT');
}

export function editAnswerText(answerId, text) {
    return authorizedWrite(defaultUrl + 'questions/answers/' + answerId + '?' + 'action=CONTENT_UPDATE', {text}, 'PUT');
}

export function addCommentForQuestion(questionId, text) {
    var url = defaultUrl + 'questions/' + questionId + '/comments';
    return authorizedWrite(url, {text}, 'POST');
}

export function addCommentForAnswer(answerId, text) {
    var url = defaultUrl + 'questions/answers/' + answerId + '/comments';
    return authorizedWrite(url, {text}, 'POST');
}

export function postAnswer(questionId, answer) {
    var url = defaultUrl + '/questions/' + questionId + '/answers';
    return authorizedWrite(url, answer, 'POST');
}

export function askQuestion(question) {
    return authorizedWrite(defaultUrl + 'questions/', question);
}

export function searchQuestions(query, skip, limit) {
    var url = defaultUrl + 'questions?text=' + query + '&' + appendSearchParam(skip, limit);
    return get(url);
}

var defaultUrl = AjaxHelper.getUrl();

var defaultToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkZXJwYXVsQGNpc2NvLmNvbSIsImZpcnN0TmFtZSI6IkRlcnJpY2siLCJsYXN0TmFtZSI6IlBhdWwiLCJpYXQiOjE0OTk3NTM5ODV9.m8YGrRUOHmVFC9xDn8AwnLAYMA9nRFedbA5srbP-tlo=";

function getToken(){
    var token = Session.getToken();
    if ('undefined' === token) {
        window.location = '/login';
    }
    return token;
}

function authorizedWrite(url, data, type='POST') {
    return new Promise((resolve, reject) => {
        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
        xmlhttp.open(type, url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlhttp.setRequestHeader("Authorization", 'bearer ' + getToken());
        xmlhttp.setRequestHeader("Accept","application/json");
        if ('undefined' !== typeof data) {
            xmlhttp.send(JSON.stringify(data))
        } else {
            xmlhttp.send();
        }

        xmlhttp.onreadystatechange=function(){
        if(xmlhttp.readyState===4)
        {
            if(xmlhttp.status===200 || xmlhttp.status===201)
            {
                var location=xmlhttp.getResponseHeader('location');
                resolve(location);
            } else {
                reject(xmlhttp.statusText);
            }
        }
        else    console.log("Response recieved with status "+xmlhttp.status);
    }});
}

function get(url, token=defaultToken) {
    return new Promise((resolve, reject) => {
        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
        xmlhttp.open("GET", url, true);
        xmlhttp.setRequestHeader("Authorization", 'bearer ' + token);
        xmlhttp.setRequestHeader("Accept","application/json");
        xmlhttp.send();

        xmlhttp.onreadystatechange=function(){
        if(xmlhttp.readyState===4)
        {
            if(xmlhttp.status===200 || xmlhttp.status===201)
            {
                var response=JSON.parse(xmlhttp.responseText);
                resolve(response);
            } else {
                reject(xmlhttp.statusText);
            }
        }
        else    console.log("Response recieved with status "+xmlhttp.status);
    }});
} 