import delay from './delay';

const questions = [{
    _id: "12345",
    title: "How do I use React, Redux, React Router, Webpack, Thunk and Babel (Wheew that was a mouthfull) to create a React application",
    text: "I've been going through the stack required to begin production application development using React and it seems utterly impossible to fix on one stack.",
    userId: "arunjoh@cisco.com",
    userName: "Arun John",
    tags: [
        'react',
        'redux',
        'babel',
        'react router',
        'thunk',
        'webpack'
    ],
    voteCount: 11,
    viewCount: 3, 
    createdDate: 12345566,
    answers : [{
        answerId: "answer-1",
        text: "Ahh ... the question that seems to be on <bold>every</bold> ones mind. <br/> Let me write some code here <br> <code>First line of code <br/> Second line of code<code>",
        userId: "derrpaul@cisco.com",
        voteCount: 234,
        comments : [{
        text: "This is the first comment.",
        userId: "johndoe@cisco.com",
        createdDate: 12312312,
    }, {
        text: "This is a really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really long comment.",
        userId: "jacksdawson@cisco.com",
        createdDate: 73764,
        
    }] 
    }],
    comments : [{
        text: "This is the first comment.",
        userId: "johndoe@cisco.com",
        createdDate: 12312312,
    }, {
        text: "This is a really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really long comment.",
        userId: "jacksdawson@cisco.com",
        createdDate: 73764,
        
    }] 
}, {
    _id: "12345678",
    title: "What is the best place on the internet to learn React",
    text: "I'd like to get started learning React. Is there some website, book or tutorial that any one would recommend that could help me get started quicklu.'",
    userId: "derrpaul@cisco.com",
    userName: "Derrick Paul",
    tags: [
        'react',
        'tutorials'
    ],
    voteCount: 3,
    viewCount: 4,
    createdDate: 123,
    answers: []
}, {
    _id: "12345678910",
    title: "Who wants to help me kill the guy who invented React",
    text: "I'm scouring the internet looking for a parnter in crime who will help me torture and kill the moron who invented React and made life so miserable.",
    userId: "nandan@cisco.com",
    userName: "Nandhan Devaraju",
    tags: [
        'react',
        'murder',
        'crime',
        'exasperation'
    ],
    voteCount: 121232,
    viewCount: 112321,
    createdDate: 3344,
    comments: [],
    answers: []
}, {
    _id: "333433",
    title: "Seriously lets kill that SBO",
    text: "I'm scouring the internet looking for a parnter in crime who will help me torture and kill the moron who invented React and made life so miserable.",
    userId: "john@cisco.com",
    userName: "John Doe",
    tags: [
        'react',
        'murder',
        'crime',
        'exasperation'
    ],
    voteCount: 3434,
    viewCount: 123,
    createdDate: 7677,
    answers: [],
    comments: []
}, {
    _id: "dfbdfb",
    title: "Why did the chicken cross the road",
    text: "Saw a chicken cross the road the other day. For the life of me I can't imagine why it would do that. Isn't it afraid of getting hit. What if it is never able to come back. Some one please help",
    userId: "nandan@cisco.com",
    userName: "Nandhan Devaraju",
    tags: [
        'react',
        'murder',
        'crime',
        'exasperation'
    ],
    voteCount: 121232,
    viewCount: 112321,
    createdDate: 3344,
    answers: [{
        answerId: "df4252",
        text: "Elementary my dear. It was to get to the other side"
    }]
}, {
    _id: "bthh",
    title: "To be or not to be that is the question",
    text: "Well the title says it all.",
    userId: "nandan@cisco.com",
    userName: "Nandhan Devaraju",
    tags: [
        'react',
        'murder',
        'crime',
        'exasperation'
    ],
    voteCount: 121232,
    viewCount: 112321,
    createdDate: 3344,
    answers: [{
        answerId: "df4252",
        text: "Well at least you have the question down pat."
    }]
}, {
    _id: "45345g",
    title: "Why do stars shine at night",
    text: "Why do stars shine at night? Do they have something against the day?",
    userId: "nandan@cisco.com",
    userName: "Nandhan Devaraju",
    tags: [
        'react',
        'murder',
        'crime',
        'exasperation'
    ],
    voteCount: 121232,
    viewCount: 112321,
    createdDate: 3344,
    answers: []
}, {
    _id: "sdfsdfsf",
    title: "Who wants to help me kill the guy who invented React",
    text: "I'm scouring the internet looking for a parnter in crime who will help me torture and kill the moron who invented React and made life so miserable.",
    userId: "nandan@cisco.com",
    userName: "Nandhan Devaraju",
    tags: [
        'react',
        'murder',
        'crime',
        'exasperation'
    ],
    voteCount: 121232,
    viewCount: 112321,
    createdDate: 3344,
    answers: []
}];

function getData(skip, limit, sorted) {
    return ({
        total: sorted.length,
        data: getQuestions(skip, limit, sorted),
    });
}

function getQuestions(skip=0, limit=3, sorted) {
    return sorted.slice(skip, skip + limit);
}

export function getNewestQuestions(skip, limit) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            var sorted = questions.sort((question1, question2) => {
                return  question2.createdDate - question1.createdDate;
            });
            resolve(getData(skip, limit, sorted));
        },
        delay
        );
    });
}

export function getMostVotedQuestions(skip, limit) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var sorted = questions.sort((question1, question2) => {
                    return  question2.voteCount - question1.voteCount;
                });
                resolve(getData(skip, limit, sorted));
            },
            delay
            );
        });
}

export function getUnansweredQuestions(skip, limit) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var sorted = questions.filter((question) => {
                    return question.answers.length === 0;
                });
                resolve(getData(skip, limit, sorted));
            },
            delay
            );
        });
}

export function getQuestionById(questionId) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    var question = Object.assign({}, filtered[0]);
                    resolve(question);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });
}

export function voteUpQuestion(questionId) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    filtered[0].voteCount++;
                    resolve(filtered[0]);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });
}

export function voteDownQuestion(questionId) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    filtered[0].voteCount--;
                    resolve(filtered[0]);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });
}

export function voteUpAnswer(questionId, answerId) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    var answers = filtered[0].answers.filter((answer) => {
                        return answer.answerId === answerId;
                    });
                    answers[0].voteCount++;
                    resolve(answers[0]);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });
}

export function voteDownAnswer(questionId, answerId) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    var answers = filtered[0].answers.filter((answer) => {
                        return answer.answerId === answerId;
                    });
                    answers[0].voteCount--;
                    resolve(answers[0]);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });        
}

export function editQuestionText(questionId, text) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    filtered[0].text = text; 
                    resolve(filtered[0].text);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });       
}

export function editAnswerText(answerId, text) {
       
}

export function addCommentForQuestion(questionId, comment) {
     return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    filtered[0].comments.push({
                        text: comment,
                        userId: 'arunjoh@cisco.com',
                        createdDate: 8989898
                    }); 
                    resolve(filtered[0].text);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });    
}

export function postAnswer(questionId, answer) {
     return new Promise((resolve, reject) => {
            setTimeout(() => {
                var filtered = questions.filter((question) => {
                    return question._id === questionId;
                });
                if (filtered.length === 1) {
                    filtered[0].answers.push({
                        answerId: "answer-1",
                        text: answer,
                        userId: "derrpaul@cisco.com",
                        voteCount: 234,
                        comments : [{
                        text: "This is the first comment.",
                        userId: "johndoe@cisco.com",
                        createdDate: 12312312,
                    }]}); 
                    resolve(filtered[0].text);
                } else {
                    reject("Question not found...");
                }
            },
            delay
            );
        });    
}

export function askQuestion(question) {
    return new Promise((resolve, reject) => {
            setTimeout(() => {
                question._id = new Date().getMilliseconds().toString();
                question.comments = [];
                question.answers = [];
                question.voteCount = 0;
                question.createdDate = new Date().getMilliseconds();
                question.userId = 'arunjoh@cisco.com'

                questions.push(question);
                resolve('/questions/' + question._id);
            },
            delay
            );
        });
}
