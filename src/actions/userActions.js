import * as types from './actionTypes';
import userApi from '../api/userApi';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import Session from '../store/session';
import toastr from 'toastr';


export function loadUsersSuccess(users){
   // debugger;
    return {type:types.LOAD_USERS_SUCCESS, users}
}

export function createUserSuccess(user){
   // debugger;
    return {type:types.CREATE_USER_SUCCESS, user}
}

export function updateUserSuccess(user){
   // debugger;
    return {type:types.UPDATE_USER_SUCCESS, user}
}

export function loadUserByIdSuccess(user) {
    debugger;
    return { type: types.LOAD_USER_SUCCESS, user }
}


export function loginUserSuccess(response) {
    debugger;
    return { type: types.LOGIN_USER_SUCCESS, response }
}

export function authenticationFailed() {
    debugger;
    return { type: types.LOGIN_USER_FAILED }
}

//one can create loadUsersFailure(){} instead of throwing

export function loadUsers(){
    return function (dispatch){
        
        dispatch(beginAjaxCall());
        
        //we can make REST call here.
        console.log(userApi.getAllUsers());
        return userApi.getAllUsers().then(users=>{
            dispatch(loadUsersSuccess(users));
        }).catch(error=>{
           throw(error); 
        });
    };
}

export function loadUserById(userId){
     return function(dispatch) {
   //     dispatch(loadingUser(""));
         debugger;
        return userApi.getUserById(userId).then(userId => {
            dispatch(loadUserByIdSuccess(userId));            
        }).catch(error => {
            throw(error);
        });
    }
}
      //  this.props.actions.loadQuestionById(this.props.match.params.questionId);

export function saveUser(user){
    return function (dispatch, getState){
        
        dispatch(beginAjaxCall());
        
        //we can make REST call here.
        return userApi.saveUser(user).then(savedUser=>{
            user.id ?  dispatch(updateUserSuccess(savedUser)):
            dispatch(createUserSuccess(savedUser));
        }).catch(error=>{
           dispatch(ajaxCallError(error));
           throw(error); 
        });
    };
}

export function loginUser(user){
    return function (dispatch){
            
        //we can make REST call here.
        return userApi.loginUser(user).then(user=>{
            Session.storeToken(user.jwtToken);
            Session.storeUser(user);
            debugger;            
            console.log(user);
            dispatch(loginUserSuccess(user));           
        }).catch(error=>{
            debugger;
            if(error.status == 401) {
                //authenticationFailed();
                dispatch(ajaxCallError('Authentication failed'));
            }  else {
                dispatch(ajaxCallError(error.toString()));
            }
                
           throw(error); 
        });
    };
}

export function createUser(user){
    return function (dispatch){
            
        //we can make REST call here.
        return userApi.createUser(user).then(user=>{
            Session.storeToken(user.jwtToken);
            Session.storeUser(user);
            debugger;            
            console.log(user);
            dispatch(loginUserSuccess(user));           
        }).catch(error=>{
            debugger;
            if(error.status && error.status === 409){
                toastr.error('userId is taken, please try new User ID');
            }else if (error.status && error.status === 422) {
                toastr.error('Mandatory fields missing, please enter all mandatory fields');
            }
              dispatch(ajaxCallError(error));     
           
           throw(error); 
        });
    };
}