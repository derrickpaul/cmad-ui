import * as types from './actionTypes';
import * as questionsApi from '../api/questionApi';
import {beginAjaxCall} from './ajaxStatusActions';

export function loadingQuestion(error) {
    return { type: types.LOADING_QUESTION, error };
}

export function loadQuestionsSuccess(questions) {
    return { type: types.LOAD_QUESTIONS_SUCCESS, questions }
}

export function loadingQuestions(error) {
    return { type: types.LOADING_QUESTIONS, error };
}

export function loadQuestionSuccess(question) {
    return { type: types.LOAD_QUESTION_SUCCESS, question }
}

export function postingQuestion(error) {
    return { type: types.POSTING_QUESTION, error };
}

export function createQuestionSuccess(questionPath) {
    return { type: types.CREATE_QUESTION_SUCCESS, questionPath }
}

export function postingComment(error) {
    return { type: types.POSTING_COMMENT, error };
}

export function postedComment(error) {
    return { type: types.POST_COMMENT_SUCCESS, error };
}

export function editingQuestion(error) {
    return { type: types.EDITING_QUESTION, error };
}

export function editQuestionSuccess(error) {
    return { type: types.EDITING_QUESTION_SUCCESS, error };
}

export function postingAnswer(error) {
    return { type: types.POSTING_ANSWER, error };
}

export function postedAnswer(error) {
    return { type: types.POST_ANSWER_SUCCESS, error };
}

export function editingAnswer(error) {
    return { type: types.EDITING_ANSWER, error };
}

export function editAnswerSuccess(error) {
    return { type: types.EDITING_ANSWER_SUCCESS, error };
}

export function searchingQuestions(error) {
    return { type: types.SEARCH_QUESTIONS, error };
}

export function loadNewestQuestions(skip, limit) {
    return function(dispatch) {
        dispatch(loadingQuestions(""));

        return questionsApi.getNewestQuestions(skip, limit).then(questions => {
            dispatch(loadQuestionsSuccess(questions))
        }).catch(error => {
            throw(error);
        });
    }
}

export function loadVotedQuestions(skip, limit) {
    return function(dispatch) {
        dispatch(loadingQuestions(""));

        return questionsApi.getMostVotedQuestions(skip, limit).then(questions => {
            dispatch(loadQuestionsSuccess(questions))
        }).catch(error => {
            throw(error);
        });
    }
}

export function loadMostViewedQuestions(skip, limit) {
    return function(dispatch) {
        dispatch(loadingQuestions(""));

        return questionsApi.getMostViewedQuestions(skip, limit).then(questions => {
            dispatch(loadQuestionsSuccess(questions))
        }).catch(error => {
            throw(error);
        });
    }
}

export function loadQuestionById(questionId) {
    return function(dispatch) {
        dispatch(loadingQuestion(""));

        return questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });
    }
}

export function voteUpQuestion(questionId) {
    return function(dispatch) {
        dispatch(editingQuestion(""));

        return questionsApi.voteUpQuestion(questionId).then(() => {
            questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });});
    }
}

export function voteDownQuestion(questionId) {
    return function(dispatch) {
        dispatch(editingQuestion(""));

        return questionsApi.voteDownQuestion(questionId).then(() => {
            questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });});
    }
}

export function voteUpAnswer(questionId, answerId) {
    return function(dispatch) {
        dispatch(editingAnswer(""));

        return questionsApi.voteUpAnswer(answerId).then(() => {
            questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });});
    }
}

export function voteDownAnswer(questionId, answerId) {
    return function(dispatch) {
        dispatch(editingAnswer(""));

        return questionsApi.voteDownAnswer(answerId).then(() => {
            questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });});
    }
}

export function editQuestionText(questionId, text) {
    return function(dispatch) {
        dispatch(editingQuestion(""));

        return questionsApi.editQuestionText(questionId, text).then(() => {
            questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });});
    }
}

export function editAnswerText(answerId, questionId, text) {
    return function(dispatch) {
        dispatch(editingAnswer(""));

        return questionsApi.editAnswerText(answerId, text).then(() => {
            questionsApi.getQuestionById(questionId).then(question => {
            dispatch(loadQuestionSuccess(question))
        }).catch(error => {
            throw(error);
        });});
    }
}

export function addCommentForQuestion(questionId, comment) {
    return function(dispatch) {
        dispatch(postingComment(""));

        return questionsApi.addCommentForQuestion(questionId, comment).then(() => {
            dispatch(postedComment(""))
            loadQuestionById(questionId)(dispatch)
        }).catch(error => {
            throw(error);
        });
    }
}

export function addCommentForAnswer(questionId, answerId, comment) {
    return function(dispatch) {
        dispatch(postingComment(""));

        return questionsApi.addCommentForAnswer(answerId, comment).then(() => {
            dispatch(postedComment(""))
            loadQuestionById(questionId)(dispatch)
        }).catch(error => {
            throw(error);
        });
    }
}

export function askQuestion(questionId, answer) {
    return function(dispatch) {
        dispatch(postingQuestion(""));

            return questionsApi.askQuestion(questionId, answer).then((questionPath) => {
                dispatch(createQuestionSuccess(questionPath))
            }).catch(error => {
                throw(error);
            });
        }
}

export function postAnswer(questionId, answer) {
    var ans = {text: answer};

    return function(dispatch) {
        dispatch(postingAnswer(""));

            return questionsApi.postAnswer(questionId, ans).then((answerPath) => {
                dispatch(postedAnswer(""));
                loadQuestionById(questionId)(dispatch);
            }).catch(error => {
                throw(error);
            });
        }
}

export function searchQuestions(query, skip, limit) {
    return function(dispatch) {
        dispatch(searchingQuestions(""));

            return questionsApi.searchQuestions(query, skip, limit).then((questions) => {
                dispatch(loadQuestionsSuccess(questions))
            }).catch(error => {
                throw(error);
            });
        }
}