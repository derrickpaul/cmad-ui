import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import App from './App';
import configureStore from './store/configureStore';

import {Provider} from 'react-redux';
import {loadNewestQuestions} from './actions/questionActions';
import {loadUsers} from './actions/userActions'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './static/styles/styles.css';
import '../node_modules/toastr/build/toastr.min.css';

const store = configureStore();

// store.dispatch(loadNewestQuestions());
// store.dispatch(loadUsers());

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
